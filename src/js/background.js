chrome.runtime.onInstalled.addListener(function () {
    chrome.contextMenus.create({
        "title": "Watch this later",
        "contexts": ["all"],
        "id": 'watchLater'
    });
});

chrome.contextMenus.onClicked.addListener(function (info, tab) {
    if (info.menuItemId === 'watchLater') {
        //info.linkUrl Check type
        console.log(info);
        console.log(tab);

        chrome.tabs.create({ url: chrome.extension.getURL("ui.html") });
    }
});



// "browser_action": {
//     "default_title": "some title",
//     "default_icon": "some_icon.png",
//     "popup": "ui.html"
// }