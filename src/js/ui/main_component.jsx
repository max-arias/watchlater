import React from "react";
import Player from "./player_component.jsx";
export default class extends React.Component {
  render () {
    return (
      <div>
        <p>This will be the page where you watch your videos</p>
        <Player
          url='https://vimeo.com/244969966'
          playing={false}
        />
        <Player
          url='https://www.youtube.com/watch?v=tvTRZJ-4EyI'
          playing={false}
        />
      </div>
    )
  }
};
