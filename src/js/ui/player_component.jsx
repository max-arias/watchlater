import React, { Component } from 'react'
import ReactPlayer from 'react-player'

export default class extends React.Component {

    constructor(props) {
        super(props);
        this.videoEnd = this.videoEnd.bind(this);
    }

    videoEnd() {
        console.log('video end');
    }
    render() {
        return (
            <ReactPlayer
                url={this.props.url}
                playing={this.props.playing}
                onEnded={this.videoEnd}
            />
        )    
    }
}