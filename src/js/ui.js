import '../img/icon-128.png'
import '../img/icon-34.png'

import "../css/popup.css";
import Main from "./ui/main_component.jsx";
import React from "react";
import { render } from "react-dom";

console.log(Main);

render(
    <Main />,
    window.document.getElementById("app-container")
);